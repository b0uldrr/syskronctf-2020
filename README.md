## Syskron CTF 2020

* **CTF Time page:** https://ctftime.org/event/1148 
* **Category:** Jeopardy
* **Date:** Wed, 21 Oct. 2020, 00:00 UTC — Mon, 26 Oct. 2020, 00:00 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
| bash history | forensics | 200 | base64 |
| change | rev, forensics | 200 | javascript, console | 
| keygen | rev | 300 | ghidra, static analysis, octal, ascii |
| security | misc | 200 | gpg, pgp |
