## Keygen

* **CTF:** Syskron CTF 2020
* **Category:** Rev
* **Points:** 300 
* **Author(s):** b0uldrr
* **Date:** 25/10/20

---

### Challenge
```
This is our official key generator that we use to derive keys from machine numbers. Our developer put a secret in its code. Can you find it?
```

### Downloads
* [keygen](keygen)

---

### Solution

The provided binary asks for the user to input a "machine number" and then generates a key for that input:
![keygen1](images/keygen1.png)

Opening the binary in Ghidra, we can see that the `main` function checks that the user input is 7 bytes long before passing the input to `genserial`:
![main](images/main.png)

Genserial first checks whether the input is equal to `0x6c61736b612121`. If it is, then it calls the function `octal()`. If not, it continues with generating the key for the user input, which is the expected functionality of the program.
![genserial](images/genserial.png)

![octal](images/octal.png)

Decoding `0x6c61736b612121` to ascii gives us a value of `laska!!`. If we run the program and input that string, the program prints out a long string of numbers with the hint that we're not done yet:
![keygen2](images/keygen2.png)


This string of numbers is the octal representation of the flag, separated by 9s.
```
1639171916391539162915791569103912491069173967911091119123955915191639156967955916396391439125916296395591439609104911191169719175
```

If we remove all of the 9s and then convert them from octal to ascii, we get the flag.

```
163 171 163 153 162 157 156 103 124 106 173 67 110 111 123 55 151 163 156 67 55 163 63 143 125 162 63 55 143 60 104 111 116 71 175
```

![flag](images/flag.png)

---

### Flag 
```
syskronCTF{7HIS-isn7-s3cUr3-c0DIN9}
```
