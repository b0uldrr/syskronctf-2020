## Security

* **CTF:** Syskron CTF 2020
* **Category:** Misc 
* **Points:** 200 
* **Author(s):** b0uldrr
* **Date:** 25/10/20

---

### Challenge
```
The security.txt draft got updated (https://tools.ietf.org/html/draft-foudil-securitytxt-10).

Is Senork's file still up-to-date? https://www.senork.de/.well-known/security.txt
```

### Downloads
* [security.txt](security.txt)
* [openpgp.asc](opengpg.asc)

---

### Solution

Opening `security.txt`, we find the following PGP signed message:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

# Canonical URL
Canonical: https://www.senork.de/.well-known/security.txt

# Our security policy
Policy: https://www.senork.de/security/

# Our security acknowledgments page
Acknowledgments: https://www.senork.de/security/#acknowledgments

# Our security address
mailto:psirt@bb-industry.cz

# Our OpenPGP key
Encryption: https://www.senork.de/openpgp.asc

# Preferred languages
Preferred-Languages: en, cs

# Expiring date of this file
Expires: Thu, 31 Dec 2020 20:00:00 +0100
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQQb0Dqaer1Y3W4NxowpcUAVAB/owgUCX1IefAAKCRApcUAVAB/o
wspjAQDDgE/cHebpoJQKIFVQukVWoNThA+53Pv7nHaZg2e9KvQD+Lroerub4IjPE
7941IBbFnsiYR9eObsAyh6+sLxZRrwc=
=q4VU
-----END PGP SIGNATURE-----
```

I tried to verify the message but found I didn't have the public key:
![verify](images/verify.png)

A link to the public key was included inside `security.txt` - https://www.senork.de/openpgp.asc. I downloaded `openpgp.asc`:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEX1IeNRYJKwYBBAHaRw8BAQdAGkGzrffXJoSEuPxIZ+ADdMAH1COdISkwrmFC
ZyCWGP+0X0JCIEluZHVzdHJ5IGEucy4gUFNJUlQgKHN5c2tyb25DVEZ7V2gwLXB1
dDMtZmxhZzMtMW50by0wcGVuUEdQLWtleTM/Pz99KSA8cHNpcnRAYmItaW5kdXN0
cnkuY3o+iJYEExYIAD4WIQQb0Dqaer1Y3W4NxowpcUAVAB/owgUCX1IeNQIbAwUJ
AE8aAAULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRApcUAVAB/owkYsAP9uMtdg
0YInW+JgxdZbGhP7dQS7Bv1fKARx2GDcVUt7BAD/cgkM1BSC3jT1PuutPA7HDwC7
709QGbka8o/G1t9EBwE=
=mLiy
-----END PGP PUBLIC KEY BLOCK-----
```
I imported it into gpg with `gpg --import openpgp.asc`. The flag was displayed in the key information:
![flag](images/flag.png)

Note that the same information is displayed when you verify the signed pgp message with the key:
![flag2](images/flag2.png)

---

### Flag 
```
syskronCTF{Wh0-put3-flag3-1nto-0penPGP-key3???}
```
